#include "input_formats.h"

#include <stdbool.h>

#include "error_handlers.h"

#define BITS_PER_BYTE 8
#define BMP_WIDTH_MULTIPLICITY 4
#define BMP_LITTLE_ENDIAN_CODE 0x4D42
#define BMP_BF_SIZE 14
#define BMP_RESERVED_VALUE 0
#define BMP_BI_PLANES_VALUE 1
#define BMP_BI_RGB 0
#define BMP_BI_SIZE_IMAGE_IF_RGB_COMPRESSION 0
#define BMP_UNDEFINED_PELS_PER_METER 0
#define BMP_COLORS_TABLE_SIZE 0


uint32_t calculate_padding_from_header(const struct bmp_header* header) {
    return BMP_WIDTH_MULTIPLICITY - ((header->bi_width * header->bi_bit_count) / BITS_PER_BYTE) % BMP_WIDTH_MULTIPLICITY;
}


uint32_t calculate_padding_from_image(const struct image* image) {
    return - (sizeof(struct pixel) * image->width) % BMP_WIDTH_MULTIPLICITY;
}


enum bmp_read_status read_bmp_header(struct image_file* image_file, struct bmp_header* header) {
    if (!image_file_read(header, sizeof(struct bmp_header), 1, image_file) || !image_file_seek(image_file, header->bf_off_bits, SEEK_SET)) return BMP_READ_BAD_HEADER;
    return BMP_READ_OK;
}

enum bmp_read_status read_bmp_pixels(struct image_file* image_file, struct bmp_header* header, struct pixel* pixels) {
    uint32_t padding_size = calculate_padding_from_header(header);

    size_t shift = 0;
    for (size_t i=0; i<header->bi_height; i++) {
        if (!image_file_read(pixels+shift, sizeof(struct pixel), header->bi_width, image_file) || !image_file_seek(image_file, padding_size, SEEK_CUR)) return BMP_READ_BAD_PIXELS;
        shift = shift + header->bi_width;
    }

    return BMP_READ_OK;
}

enum bmp_read_status read_bmp(struct image_file* image_file, struct image* image) {
    struct bmp_header header;
    if (read_bmp_header(image_file, &header)) return BMP_READ_BAD_HEADER;
    struct pixel* pixels = allocate_pixels_memory(header.bi_width, header.bi_height);
    handle_allocate_pixels_memory_error(pixels);
    if (read_bmp_pixels(image_file, &header, pixels)) return BMP_READ_BAD_PIXELS;
    image->width = header.bi_width;
    image->height = header.bi_height;
    image->pixels = pixels;

    return BMP_READ_OK;
}



void bmp_fill_header(struct image* image, struct bmp_header* header) {
    header->bf_type = BMP_LITTLE_ENDIAN_CODE;
    size_t padding_size =  - (sizeof(struct pixel) * image->width) % BMP_WIDTH_MULTIPLICITY;
    header->bf_file_size = sizeof(struct bmp_header) + (image->width + padding_size) * image->height;
    header->bf_reserved = BMP_RESERVED_VALUE;
    header->bf_off_bits = sizeof(struct bmp_header);
    header->bi_size = sizeof(struct bmp_header) - BMP_BF_SIZE;
    header->bi_width = image->width;
    header->bi_height = image->height;
    header->bi_planes = BMP_BI_PLANES_VALUE;
    header->bi_bit_count = sizeof(struct pixel) * BITS_PER_BYTE;
    header->bi_compression = BMP_BI_RGB;
    header->bi_size_image = BMP_BI_SIZE_IMAGE_IF_RGB_COMPRESSION;
    header->bi_x_pels_per_meter = BMP_UNDEFINED_PELS_PER_METER;
    header->bi_y_pels_per_meter = BMP_UNDEFINED_PELS_PER_METER;
    header->bi_clr_used = BMP_COLORS_TABLE_SIZE;
    header->bi_clr_important = BMP_COLORS_TABLE_SIZE;
}

enum bmp_write_status bmp_write_header(struct image_file* image_file, struct bmp_header* header) {
    if (!image_file_write(header, sizeof(struct bmp_header), 1, image_file)) return BMP_WRITE_HEADER_ERR;
    return BMP_WRITE_OK;
}

enum bmp_write_status bmp_write_pixels(struct image_file* image_file, struct image* image) {
    uint32_t padding_size = calculate_padding_from_image(image);

    size_t shift = 0;
    for (size_t i=0; i<image->height; i++) {
        if (!image_file_write(image->pixels+shift, sizeof(struct pixel), image->width, image_file) || !image_file_seek(image_file, padding_size, SEEK_CUR)) return BMP_WRITE_PIXELS_ERR;
        shift += image->width;
    }
    
    return BMP_WRITE_OK;
}

enum bmp_write_status write_bmp(struct image_file* image_file, struct image* image) {
    struct bmp_header header;
    bmp_fill_header(image, &header);
    if (bmp_write_header(image_file, &header)) return BMP_WRITE_HEADER_ERR;

    if (bmp_write_pixels(image_file, image)) return BMP_WRITE_PIXELS_ERR;

    return BMP_WRITE_OK;
}
