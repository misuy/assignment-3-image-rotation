#include "error_handlers.h"


void handle_parse_arguments_error(int status) {
    switch (status) {
        case PARSE_ARGUMENTS_OK:
            return;
        case PARSE_ARGUMENTS_BAD_ARGUMENTS_COUNT:
            fprintf(stderr, "Bad arguments count. Shoud use: ./image_transformer {input_path} {output_path}.\n");
            abort();
        default:
            fprintf(stderr, "Something wrong with arguments.");
            abort();
    }
}


void handle_open_image_file_error(struct maybe_image_file* maybe_image_file) {
    switch (maybe_image_file->open_status) {
        case IMAGE_FILE_OPEN_OK:
            return;
        case IMAGE_FILE_OPEN_BAD_PATH:
            fprintf(stderr, "Cannot open image file (path=%s). Bad path.\n", (maybe_image_file->image_file).path);
            abort();
        case IMAGE_FILE_OPEN_ACCESS_DENIED:
            fprintf(stderr, "Cannot open image file (path=%s). Access denied.\n", (maybe_image_file->image_file).path);
            abort();
        default:
            fprintf(stderr, "Cannot open image file (path=%s). Something went wrong.\n", (maybe_image_file->image_file).path);
            abort();
    }
}


void handle_close_image_file_error(struct image_file* image_file, bool status) {
    if (!status) {
        fprintf(stderr, "Cannot close image file (path=%s).\n", image_file->path);
        abort();
    }
}


void handle_allocate_pixels_memory_error(struct pixel* pixels) {
    if (!pixels) {
        fprintf(stderr, "Cannot allocate pixels memory.\n");
        abort();
    }
}


void handle_read_bmp_error(struct image_file* image_file, enum bmp_read_status status) {
    switch (status) {
        case BMP_READ_OK:
            return;
        case BMP_READ_BAD_HEADER:
            fprintf(stderr, "Cannot read bmp file header (path=%s).)", image_file->path);
            abort();
        case BMP_READ_BAD_PIXELS:
            fprintf(stderr, "Cannot read bmp file pixels (path=%s).)", image_file->path);
            abort();
        default:
            fprintf(stderr, "Cannot read bmp file (path=%s).)", image_file->path);
            abort();
    }
}


void handle_write_bmp_error(struct image_file* image_file, enum bmp_write_status status) {
    switch (status) {
        case BMP_WRITE_OK:
            return;
        case BMP_WRITE_HEADER_ERR:
            fprintf(stderr, "Cannot write bmp file handler (path=%s).)", image_file->path);
            abort();
        case BMP_WRITE_PIXELS_ERR:
            fprintf(stderr, "Cannot write bmp file pixels (path=%s).)", image_file->path);
            abort();
        default:
            fprintf(stderr, "Cannot write bmp file (path=%s).)", image_file->path);
            abort();
    }
}
