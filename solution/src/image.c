#include "image.h"

struct image create_image(void) {
    struct image image = {.width = 0, .height = 0, .pixels = NULL};
    return image;
}

struct pixel* allocate_pixels_memory(uint64_t width, uint64_t height) {
    struct pixel* pixels = malloc(sizeof(struct pixel) * width * height);
    return pixels;
}

void free_image_memory(struct image* image) {
    free(image->pixels);
}
