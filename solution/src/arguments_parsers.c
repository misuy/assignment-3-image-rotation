#include "arguments_parsers.h"

enum parse_arguments_status parse_input_and_output_paths(int argc, char** argv, char** input_path, char** output_path) {
    if (argc != 3) return PARSE_ARGUMENTS_BAD_ARGUMENTS_COUNT; // :)
    if ((argv[1] != NULL) & (argv[2] != NULL)) {
        *input_path = argv[1];
        *output_path = argv[2];
        return PARSE_ARGUMENTS_OK;
    }
    return PARSE_ARGUMENTS_SOMETHING_WENT_WRONG;
}
