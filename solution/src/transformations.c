#include "transformations.h"

#include "error_handlers.h"

void rotate_90_counterclockwise(const struct image* image, struct image* new_image) {
    if (new_image->pixels) free(new_image->pixels);
    struct pixel* new_pixels = allocate_pixels_memory(image->width, image->height);
    handle_allocate_pixels_memory_error(new_pixels);
    if (new_pixels) {
        for (size_t i=0; i<image->height; i++) {
            for (size_t j=0; j<image->width; j++) {
                new_pixels[(j * image->height) + (image->height - i - 1)] = (image->pixels)[(i * image->width) + j];
            }
        }
        new_image->width = image->height;
        new_image->height = image->width;
        new_image->pixels = new_pixels;
    }
}
