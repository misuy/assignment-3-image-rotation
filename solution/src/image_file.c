#include "image_file.h"

#include <errno.h>

struct image_file new_image_file(char* path) {
    return (struct image_file) {.path = path, .file = NULL};
}

struct maybe_image_file open_image_file(char* path, const char* mode) {
    struct image_file image_file;
    struct maybe_image_file maybe_image_file;

    image_file.path = path;
    image_file.file = fopen(path, mode);
    maybe_image_file.image_file = image_file;

    if (image_file.file) {
        maybe_image_file.open_status = IMAGE_FILE_OPEN_OK;
    }
    else {
        switch (errno)
        {
            case ENOENT:
                maybe_image_file.open_status = IMAGE_FILE_OPEN_BAD_PATH;
            case EACCES:
                maybe_image_file.open_status = IMAGE_FILE_OPEN_ACCESS_DENIED;
            default:
                maybe_image_file.open_status = IMAGE_FILE_OPEN_SOMETHING_WENT_WRONG;
        }
    }

    return maybe_image_file;
}

bool image_file_read(void* buffer, size_t size, size_t count, struct image_file* image_file) {
    return fread(buffer, size, count, image_file->file) == count;
}

bool image_file_write(void* buffer, size_t size, size_t count, struct image_file* image_file) {
    return fwrite(buffer, size, count, image_file->file) == count;
}

bool image_file_seek(struct image_file* image_file, uint32_t offset, int seek_pos) {
    return !fseek(image_file->file, offset, seek_pos);
}

bool close_image_file(struct image_file* image_file) {
    return !fclose(image_file->file);
}
