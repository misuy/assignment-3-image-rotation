#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "arguments_parsers.h"
#include "error_handlers.h"
#include "image.h"
#include "image_file.h"
#include "input_formats.h"
#include "transformations.h"


int main(int argc, char** argv) {
    char* input_path = NULL;
    char* output_path = NULL;
    handle_parse_arguments_error(parse_input_and_output_paths(argc, argv, &input_path, &output_path));

    struct maybe_image_file maybe_input_file = open_image_file(input_path, "rb");
    handle_open_image_file_error(&maybe_input_file);
    struct image_file input_file = maybe_input_file.image_file;
    struct image image = create_image();
    handle_read_bmp_error(&input_file, read_bmp(&input_file, &image));

    struct image rotated_image = create_image();
    rotate_90_counterclockwise(&image, &rotated_image);

    struct maybe_image_file maybe_output_file = open_image_file(output_path, "wb");
    handle_open_image_file_error(&maybe_output_file);
    struct image_file output_file = maybe_output_file.image_file;
    handle_write_bmp_error(&output_file, write_bmp(&output_file, &rotated_image));

    free_image_memory(&image);
    free_image_memory(&rotated_image);

    handle_close_image_file_error(&input_file, close_image_file(&input_file));
    handle_close_image_file_error(&output_file, close_image_file(&output_file));

    return 0;
}
