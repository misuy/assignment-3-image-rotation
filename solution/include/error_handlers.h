#ifndef _ERROR_HANDLERS_H_
#define _ERROR_HANDLERS_H_

#include <stdio.h>

#include "arguments_parsers.h"
#include "image.h"
#include "image_file.h"
#include "input_formats.h"

void handle_parse_arguments_error(int status);

void handle_open_image_file_error(struct maybe_image_file* maybe_image_file);

void handle_close_image_file_error(struct image_file* image_file, bool status);

void handle_allocate_pixels_memory_error(struct pixel* pixels);

void handle_read_bmp_error(struct image_file* image_file, enum bmp_read_status status);

void handle_write_bmp_error(struct image_file* image_file, enum bmp_write_status status);

#endif
