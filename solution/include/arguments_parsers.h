#ifndef _ARGUMENTS_PARSERS_H_
#define _ARGUMENTS_PARSERS_H_

#include <stdlib.h>

enum parse_arguments_status {
    PARSE_ARGUMENTS_OK,
    PARSE_ARGUMENTS_BAD_ARGUMENTS_COUNT,
    PARSE_ARGUMENTS_SOMETHING_WENT_WRONG
};

enum parse_arguments_status parse_input_and_output_paths(int argc, char** argv, char** input_path, char** output_path);

#endif
