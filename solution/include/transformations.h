#ifndef _TRANSFORMATIONS_H_
#define _TRANSFORMATIONS_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

#include "image.h"

void rotate_90_counterclockwise(const struct image* image, struct image* new_image);

#endif
